<?php
    $servername='localhost';
    $username='root';
    $password='';
    $dbname = "articlemanagement";
    $conn=mysqli_connect($servername,$username,$password,"$dbname");
    if(!$conn){
       die('Could not Connect My Sql:' .mysql_error());
    }

    $result = mysqli_query($conn,"SELECT * FROM tbl_article");
?>
<!DOCTYPE html>
<html lang="en">
<title>Article Management</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif;}
.w3-sidebar {
  z-index: 3;
  width: 250px;
  top: 43px;
  bottom: 0;
  height: inherit;
}

/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: blue;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}

/* The popup form - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  bottom: 0;
  right: 15px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

textarea {
  width: 100%;
  height: 150px;
  padding: 10px;
  box-sizing: border-box;
  margin-bottom: 10px;
  border: 2px solid #ccc;
  border-radius: 4px;
  background-color: #f8f8f8;
  font-size: 16px;
  resize: none;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
    <a href="#" class="w3-bar-item w3-button w3-theme-l1">Logo</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-hover-white">About</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Values</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-hover-white">News</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Contact</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-hide-medium w3-hover-white">Clients</a>
    <a href="#" class="w3-bar-item w3-button w3-hide-small w3-hide-medium w3-hover-white">Partners</a>
  </div>
</div>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <?php
            $i=0;
            while($row = mysqli_fetch_array($result)) {
            ?>
                <?php 
                $photo = 'images/'.$row["filename"];
                ?>
                <img style="border: 1px solid #ddd;
  border-radius: 4px; width: 200px; height: 150px" src="<?php echo $photo; ?>" alt="" />
               <h1 class="w3-text-teal"><?php echo $row["title"]; ?></h1>
               <h3 class="w3-text-teal"><?php echo $row["author"]; ?></h3>
               <p><?php echo $row["content"]; echo $photo; ?></p>
               <hr>
            <?php
            $i++;
            }
        ?>
      
    </div>   
  </div>

  <button class="open-button" onclick="openForm()">Add Article</button>
    <?php
       $smg = "";

      if(isset($_POST['save'])) { 

         $first_name = $_POST['title'];
         $last_name = $_POST['author'];
         $city_name = $_POST['content'];

         $target = "images/".basename($_FILES['image']['name']);
         $image = $_FILES['image']['name'];
      
          // $sql = "insert into tbl_article (author,title,content,filename) 
          //           values ('$first_name','$last_name','$city_name','$image')";
          // mysql_query($conn, $sql);

          mysqli_query($conn,"insert into tbl_article (author,title,content,filename) 
                   values ('$first_name','$last_name','$city_name','$image')") or die(mysqli_error());
          echo "<p align=center>Data Added Successfully.</p>"; 
          if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
                   $smg = "Image Upload Successfully!";
          } else {
                    $smg = "There was a Failed!!!";
          }
      }
    ?>
  <div class="form-popup" id="myForm">
    <form  action="assignment.php" method="post" class="form-container" enctype="multipart/form-data">
      <h1>Article Management</h1>

      <label ><b>Arthor</b></label>
      <input type="text" placeholder="Enter Arthor" name="author" required>

      <label ><b>Title</b></label>
      <input type="text" placeholder="Enter Title" name="title" required>

      <label ><b>Content</b></label>
      <textarea name="content">Some text...</textarea>
      
      <label ><b>Select Image File to Upload: </b></label>
      <input type="file" name="image">

      <button type="submit" class="btn" name="save">Add</button>
      <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
    </form>
  </div>


  <!-- Pagination -->
  <div class="w3-center w3-padding-32">
    <div class="w3-bar">
      <a class="w3-button w3-black" href="#">1</a>
      <a class="w3-button w3-hover-black" href="#">2</a>
      <a class="w3-button w3-hover-black" href="#">3</a>
      <a class="w3-button w3-hover-black" href="#">4</a>
      <a class="w3-button w3-hover-black" href="#">5</a>
      <a class="w3-button w3-hover-black" href="#">»</a>
    </div>
  </div>

  <footer id="myFooter">
    <div class="w3-container w3-theme-l2 w3-padding-32">
      <h4>Footer</h4>
    </div>

    <div class="w3-container w3-theme-l1">
      <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
    </div>
  </footer>

<!-- END MAIN -->
</div>

<script>

  function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
  if (mySidebar.style.display === 'block') {
    mySidebar.style.display = 'none';
    overlayBg.style.display = "none";
  } else {
    mySidebar.style.display = 'block';
    overlayBg.style.display = "block";
  }
}

// Close the sidebar with the close button
function w3_close() {
  mySidebar.style.display = "none";
  overlayBg.style.display = "none";
}
</script>

</body>
</html>